module.exports = {
	debug: {
		src : 'debug/index.html', 
		dest: 'debug/index.html',
		options: { 
			context: { dist: false } 
		}
	},
	dist: {
		src: 'public/index.html', 
		dest: 'tmp/index.html',
		options: { 
			context: { dist: true } 
		}
	}
};