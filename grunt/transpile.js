var grunt = require('grunt');

module.exports = {
	debug: {
		type: 'amd',
		moduleName: function(path) {
			return grunt.config.process('<%= pkg.namespace %>/') + path;
		},
		files: [{
			expand: true,
			cwd: 'src/',
			src: '**/*.js',
			dest: 'debug/app/'
		}]
	},
	dist: {
		type: 'amd',
		moduleName: function(path) {
			return grunt.config.process('<%= pkg.namespace %>/') + path;
		},
		files: [{
			expand: true,
			cwd: 'src/',
			src: '**/*.js',
			dest: 'tmp/app/'
		}]
	}
};
