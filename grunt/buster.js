module.exports = {
	debug: {
		test: {
			config: 'test/debug.js'
		}
	},
	dist: {
		test: {
			config: 'test/debug.js'
		}
	},
	options: {
		growl: true
	}
};