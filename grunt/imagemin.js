module.exports = {
	dist: {
		options: {
			optimizationLevel: 3
		},
		files: [
			{
				expand: true,
				cwd: 'public/assets/img',
				src: '**/*.{png,gif,jpg,jpeg}',
				dest: 'dist/assets/img/'
			},
			{
				expand: true,
				cwd: 'tmp/assets/img',
				src: '**/*.{png,gif,jpg,jpeg}',
				dest: 'dist/assets/img/'
			}
		]
	}
};
