module.exports = {

	debug: {
		files: [
			{
				expand: true,
				cwd: 'public',
				src: ['**/*'],
				dest: './debug'
			},
			{
				expand: true,
				cwd: 'config/',
				src: ['**/*.js'], 
				dest: './debug/assets/scripts/config', 
				filter: 'isFile' 
			},
			{
				expand: true,
				cwd: 'bower_components',
				src: [
					'**/*.{css,js}',
					'**/*.{png,gif,jpg,jpeg,svg}',
					'**/*.{eot,ttf,woff,otf}',
					'**/*.map' // No source maps
				],
				dest: './debug/assets/vendor',
				filter: 'isFile'
				
			},
			{
				expand: true,
				cwd: 'vendor',
				src: [
					'**/*.{css,js}',
					'**/*.{png,gif,jpg,jpeg,svg}',
					'**/*.{eot,ttf,woff,otf}',
					'**/*.map' // No source maps
				],
				dest: './debug/assets/vendor',
				filter: 'isFile'

			},
			{
				expand: true,
				cwd: 'bower_components/bootstrap/less',
				src: ['**/*.less'],
				dest: './debug/assets/styles/bootstrap/',
				filter: 'isFile'
			},
			{
				expand: true,
				cwd: 'bower_components/bootswatch/<%= swatch %>',
				src: ['bootswatch.less', 'variables.less'],
				dest: './debug/assets/styles/swatch/'
			}
		]
	},
	dist: {
		files: [
			{
				expand: true,
				cwd: 'public',
				src: [
					'**/*.less'
				], 
				dest: './tmp'
			},
			{
				expand: true,
				cwd: 'config/',
				src: ['**/*.js'], 
				dest: './tmp/assets/scripts/config', 
				filter: 'isFile' 
			},
			{
				expand: true,
				cwd: 'bower_components',
				src: [
					'**/*.{css,js}'
				],
				dest: './tmp/assets/vendor',
				filter: 'isFile'
				
			},
			{
				expand: true,
				flatten: true,
				cwd: 'bower_components',
				src: [
					'**/*.{png,gif,jpg,jpeg,svg}'
				],
				dest: './tmp/assets/img',
				filter: 'isFile'
				
			},
			{
				expand: true,
				cwd: 'vendor',
				src: [
					'**/*.{css,js}'
				],
				dest: './tmp/assets/vendor',
				filter: 'isFile'

			},
			{
				expand: true,
				flatten: true,
				cwd: 'vendor',
				src: [
					'**/*.{png,gif,jpg,jpeg,svg}'
				],
				dest: './tmp/assets/img',
				filter: 'isFile'

			},
			{
				expand: true,
				flatten: true,
				cwd: 'bower_components',
				src: [
					'**/*.{eot,ttf,woff,otf}'
				],
				dest: './dist/assets/fonts',
				filter: 'isFile'
			},
			{
				expand: true,
				cwd: 'public',
				src: [
					'*',
					'!index.html',
					'!**/*.{css,js}',
					'!**/*.{eot,ttf,woff,otf}',
					'!**/*.{png,gif,jpg,jpeg,svg}'
				],
				dest: './dist',
				filter: 'isFile'
			},
			{
				expand: true,
				cwd: 'bower_components/bootstrap/less',
				src: ['**/*.less'],
				dest: './tmp/assets/styles/bootstrap/',
				filter: 'isFile'
			},
			{
				expand: true,
				cwd: 'bower_components/bootswatch/<%= swatch %>',
				src: ['bootswatch.less', 'variables.less'],
				dest: './tmp/assets/styles/swatch/'
			}
		]
	},
	index: {
		files: [
			{
				expand: true,
				cwd: 'tmp',
				src: [
					'index.html'
				], 
				dest: './dist'
			}
		]
	}
};