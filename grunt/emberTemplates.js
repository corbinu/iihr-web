var grunt = require('grunt');

module.exports = {
	options: {
		templateBasePath: /src\//,
		templateRegistration: function(name, template) {
			return grunt.config.process("define('<%= pkg.namespace %>/") + name + "', ['exports'], function(__exports__){ __exports__['default'] = " + template + "; });";
		}
	},
	debug: {
		options: {
			precompile: false
		},
		src: "src/**/*.hbs",
		dest: "debug/assets/scripts/templates.js"
	},
	dist: {
		src: "src/**/*.hbs",
		dest: "tmp/assets/scripts/templates.js"
	}
};
