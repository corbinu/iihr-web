module.exports = {
	debug: {
		src: ['debug/app/**/*.js'],
		dest: 'debug/assets/scripts/app.js',
		options: {
			sourcesContent: true
		}
	},
	dist: {
		src: ['tmp/app/**/*.js'],
		dest: 'tmp/assets/scripts/app.js',
		options: {
			sourcesContent: true
		}
	}
};
