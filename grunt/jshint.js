module.exports = {
	src: {
		src: [ 'src/**/*.js' ]
	},
	tooling: {
		src: [ 
			'Gruntfile.js', 
			'grunt/**/*.js' 
		]
	},
	test: {
		src: [ 'test/**/*.js' ]
	},
	options: {
		force: true,
		esnext: true
	}
};
