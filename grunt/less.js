module.exports = {
	debug: {
		options: {
			compress: false
		},
		files: {
			"./debug/assets/styles/app.css": "./debug/assets/styles/build.less"
		}
	},
	dist: {
		options: {
			compress: true,
			cleancss: true
		},
		files: {
			"./tmp/assets/styles/app.css": "./tmp/assets/styles/build.less"
		}
	}
};