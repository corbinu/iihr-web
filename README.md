Inventory of Industrial Heritage Resources
==========================


Web app based on Ember JS

[![build status](https://ci.openswimsoftware.com/projects/2/status.png?ref=master)](https://ci.openswimsoftware.com/projects/2?ref=master)

## Install:

OS X

`brew install phantomjs`

`sudo npm install -g bower buster http-server grunt-cli ember-tools`

`sudo npm install && bower install`

Linux

`sudo npm install -g bower buster http-server grunt-cli ember-tools phantomjs`

`sudo npm install && bower install`

Notifications:
[node-growel](https://github.com/visionmedia/node-growl)

## Licenses

Start Bootstrap Modern Business Theme licensed under Apache 2.0. 
For more info and more free Bootstrap 3 HTML themes, visit http://startbootstrap.com!