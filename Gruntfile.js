/*global module:false*/
module.exports = function(grunt) {

	var _ = grunt.util._;
	var path = require('path');

	require("time-grunt")(grunt);

	var baseConfig = {
		swatch: "flatly",
		pkg: grunt.file.readJSON('package.json'),
		banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
			'<%= grunt.template.today("yyyy-mm-dd") %>\n' +
			'<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
			'* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
			' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
		'http-server': {
			'dev': {
				root: './debug',
				port: 3000,
				cache: -1
			},
			'prod': {
				root: './dist',
				host: "0.0.0.0",
				port: 8080,
				showDir: false
			}
		}
	};

	var config = _.extend(baseConfig,
	require('load-grunt-config')(grunt, {
			loadGruntTasks: true,
			init: false
		})
	);

	config.env = process.env;

	grunt.registerTask('build:debug', "Build debug version of the app.", [
										'clean:debug',
										'mkDebug',
										'copy:debug', 
										'concurrent:debug'
									]);

	grunt.registerTask('build:dist', "Build a minified & production-ready version of the app.", [
										'clean:dist',
										'clean:tmp',
										'mkDist',
										'mkTmp',
										'copy:dist',
										'concurrent:dist',
										'createDistVersion',
										'copy:index',
										'clean:tmp'
									]);

	grunt.registerTask('build', "Build a debug and distributable of the app.", [ 'concurrent:both' ]);



	grunt.registerTask('http:dev', 'http-server:dev');
	grunt.registerTask('http:prod', 'http-server:prod');

	grunt.registerTask('http', ['http:dev']);



	grunt.registerTask('test:ci:debug', "Run tests on debug in PhantomJS.", 'buster:debug:phantomjs');
	grunt.registerTask('test:ci:dist', "Run tests in dist in PhantomJS.", 'buster:dist:phantomjs');

	grunt.registerTask('test:browsers', "Run tests in multiple browsers.");

	grunt.registerTask('test:debug', "Build dist & test application.", ['debug', 'test:ci:debug']);
	grunt.registerTask('test:dist', "Build dist & test application.", ['dist', 'test:ci:dist']);

	grunt.registerTask('test', "Build & test application in PhantomJS.", ['build', 'test:ci:debug', 'test:ci:dist']);



	grunt.registerTask('default', ['build:debug']);


	grunt.registerTask('createDistVersion', [
										'useminPrepare', // Configures concat, cssmin and uglify
										'concat', // Combines css and javascript files

										'cssmin', // Minifies css
										'uglify', // Minifies javascript
										'imagemin', // Optimizes image compression

										'usemin' // Replaces file references
									]);

	// Parallelize most of the build process
	_.merge(config, {
		concurrent: {
			both: [
				'build:dist',
				'build:debug'
			],
			debug: [
				'buildTemplates:debug',
				'buildScripts:debug',
				'buildStyles:debug',
				'buildIndexHTML:debug'
			],
			dist: [
				'buildTemplates:dist',
				'buildScripts:dist',
				'buildStyles:dist',
				'buildIndexHTML:dist'
			]
		}
	});

	// Templates
	grunt.registerTask('buildTemplates:debug', [ 'emberTemplates:debug' ]);

	grunt.registerTask('buildTemplates:dist', [ 'emberTemplates:dist' ]);

	// Scripts
	grunt.registerTask('buildScripts:debug', [
										'jshint',
										'transpile:debug',
										'concat_sourcemap:debug'
									]);

	grunt.registerTask('buildScripts:dist', [
										'jshint',
										'transpile:dist',
										'concat_sourcemap:dist'
									]);

	// Styles
	grunt.registerTask('buildStyles:debug', [ 'less:debug' ]);

	grunt.registerTask('buildStyles:dist', [ 'less:dist' ]);

	// Index HTML
	grunt.registerTask('buildIndexHTML:debug', [ 'preprocess:debug' ]);

	grunt.registerTask('buildIndexHTML:dist', [ 'preprocess:dist' ]);
	
	grunt.registerTask('mkTmp', function() {
		grunt.file.mkdir('tmp/assets/styles');
		grunt.file.mkdir('tmp/assets/img');
	});

	grunt.registerTask('mkDebug', function(base) {
		grunt.file.mkdir('debug/assets/styles');
		grunt.file.mkdir('debug/assets/img');
	});

	grunt.registerTask('mkDist', function(base) {
		grunt.file.mkdir('dist/assets/img');
	});

	grunt.initConfig(config);
};
