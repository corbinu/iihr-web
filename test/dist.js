var config = module.exports;

config["Debug Tests"] = {
    environment: "browser",  // or "node"
    rootPath: "../",
    sources: [
        "dist/assets/scripts/main.js",
        "config/environment.js",
        "config/environments/test.js"
    ],
    tests: [
        "test/**/*-test.js"
    ]
};