buster.testCase("Bower", {

	"Require jQuery": function () {
		var $ = require('jquery');
		buster.assert.isFunction($);
	},

	"Require Handlebars": function () {
		var Handlebars = require('handlebars');
		buster.assert.isObject(Handlebars);
	},

	"Require Bootstrap": function () {
		require('bootstrap');
		buster.assert.isFunction($().alert);
	},

	"Require Select2": function () {
		require('select2');
		buster.assert.isFunction($().select2);
	},

	"Require Ember": function () {
		var Ember = require('ember');
		buster.assert.isObject(Ember);
	},

	"Require Ember Data": function () {
		require('ember-data');
		buster.assert.isObject(DS);
	}

});