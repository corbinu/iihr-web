export default Ember.Handlebars.makeBoundHelper(function(word) {
	if (word.length < 247) {
		return word;
	} else {
		return word.slice(0, 247) + "...";
	}
});