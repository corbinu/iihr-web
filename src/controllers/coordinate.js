export default Ember.ObjectController.extend({
	lat: function() {
		var coordinates = this.get("model");
		return coordinates[0];
	}.property('model'),
	long: function() {
		var coordinates = this.get("model");
		return coordinates[1];
	}.property('model'),

	actions: {

		editCoordinate: function () {
			this.set('isEditing', true);
		},

		acceptChanges: function () {
			this.set('isEditing', false);

			if (Ember.isEmpty(this.get('model'))) {
				this.send('removeCoordinate');
			}
		},

		removeCoordinate: function () {
			var parent = this.get("target.parentController");
			parent.send('removeCoordinate', this.get('model'));
		}

	},

	isEditing: false

});