export default Ember.ObjectController.extend({
	actions: {

		editThreat: function () {
			this.set('isEditing', true);
		},

		acceptChanges: function () {
			this.set('isEditing', false);
		},

		removeThreat: function () {
			var parent = this.get("target.parentController");
			parent.send('removeThreat', this.get('model'));
		}

	},

	isEditing: false

});