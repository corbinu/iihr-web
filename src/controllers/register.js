export default Ember.ObjectController.extend({

	actions: {
		add: function() {
			var _this = this;
			var user = this.store.createRecord('user', this.get('model'));
			user.save().then(function () {
				_this.transitionToRoute('signin');
			});
		}
	}

});