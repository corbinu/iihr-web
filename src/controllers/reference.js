export default Ember.ObjectController.extend({

	actions: {

		editReference: function () {
			this.set('isEditing', true);
		},

		acceptChanges: function () {
			this.set('isEditing', false);

			if (Ember.isEmpty(this.get('model'))) {
				this.send('removeReference');
			}
		},

		removeReference: function () {
			var parent = this.get("target.parentController");
			parent.send('removeReference', this.get('model'));
		}

	},

	isEditing: false

});