export default Ember.ObjectController.extend({
	errorMessage: false,

	actions: {

		save: function() {
			var _this = this;
			var model = this.get('model');

			this.set("errorMessage", false);
			var password = this.get('password');

			if (password && password !== "") {
				if (this.get('confirm') !== password) {
					this.set("errorMessage", "Password does not match confirm");
					this.set("password", "");
					this.set("confirm", "");
				} else {
					model.set('password', this.get('password'));
				}
			}

			model.save().then(function () {
				_this.set('saved', true);
				_this.transitionToRoute('user', model.get("id"));
			});

		}
	}

});