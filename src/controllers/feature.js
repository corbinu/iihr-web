export default Ember.ObjectController.extend({
	types: [
		"Point",
		"Polygon"
	],
	formats: [
		"UTM",
		"Lat/Long"
	],
	format: "Lat/Long",
	isUTM: function() {
		return (this.get("format") === "UTM");
	}.property('format'),

	isPoint: function() {
		var model = this.get('model');
		return (this.get('model.type') === "Point");
	}.property('model.type'),

	lat: function() {
		if (this.get("isPoint")) {
			var coordinates = this.get("model.coordinates");
			return coordinates[0];
		}
	}.property('model.coordinates'),
	long: function() {
		if (this.get("isPoint")) {
			var coordinates = this.get("model.coordinates");
			return coordinates[1];
		}
	}.property('model.coordinates'),

	coordinates: function() {
		if ( ! this.get("isPoint")) {
			return this.get("model.coordinates");
		}
	}.property('model.coordinates'),

	actions: {

		editFeature: function () {
			this.set('isEditing', true);
		},

		acceptChanges: function () {
			this.set('isEditing', false);

			if (Ember.isEmpty(this.get('model'))) {
				this.send('removeFeature');
			}
		},

		removeFeature: function () {
			var parent = this.get("target.parentController");
			parent.send('removeFeature', this.get('model'));
		},

		removeCoordinate: function(coordinate) {
			this.get("model.coordinates").removeObject(coordinate);
		},

		createCoordinate: function () {
			var coordinates;
			if (this.get("isUTM")) {
				var zone = this.get('zone');
				coordinates = WGS84Util.UTMtoLL({
					geometry: {
						coordinates: [ this.get('easting'), this.get('northing') ]
					},
					properties: {
						zoneLetter: zone.substring(zone.length-1, zone.length),
						zoneNumber: zone.substring(0, zone.length-1)
					}
				}).coordinates;
			} else {
				coordinates = [ this.get("lat"), this.get("long") ];
			}

			if ( ! this.get('model.location.geojson.features')) {
				this.get('model.location.geojson.features', []);
			}

			this.get('model.coordinates').addObject(coordinates);

			this.set('zone', '');
			this.set('easting', '');
			this.set('northing', '');
			this.set('lat', '');
			this.set('long', '');
		}

	},

	isEditing: false

});