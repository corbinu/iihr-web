export default Ember.ObjectController.extend({
	link: function() {
		var src = this.get("model.src");
		if (window.ENV.host) {
			return window.ENV.host + "/api/v" + window.ENV.api_version + "/file/" + src;
		} else {
			return "/api/v" + window.ENV.api_version + "/file/" + src;
		}
	}.property('model.src'),

	actions: {

		removeDocument: function () {
			this.get("parentController").send('removeDocument', this.get('model'));
		}

	}

});