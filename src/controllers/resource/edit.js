export default Ember.ObjectController.extend({
	authorUser: function() {
		return this.store.find('user', this.get('model.author'));
	}.property('model.author'),
	features: function() {
		return this.get("model.location.geojson.features");
	}.property('model.location.geojson.features'),
	formats: [
		"UTM",
		"Lat/Long"
	],
	format: "Lat/Long",
	isUTM: function() {
		return (this.get("format") === "UTM");
	}.property('format'),
	featureTypes: [
		"Point",
	    "Polygon"
	],
	featureType: "Point",
	isPoint: function() {
		return (this.get("featureType") === "Point");
	}.property('featureType'),
	conditions: [
		"",
		"Good",
		"Fair",
		"Poor",
		"Very Bad",
		"Destroyed/Demolished"
	],
	types: [
		"",
		"Archaeological Heritage (Element)",
		"Archaeological Heritage (Site)",
		"Architectural Heritage",
		"Engineering Heritage",
		"Machinery",
		"Landscape"
	],
	classifications: [
		"",
		"0. EXTRACTIVE INDUSTRIES (EXTRAC)",
		"1. BULK PRODUCTS INDUSTRIES (BULK)",
		"2. MANUFACTURING -INDUSTRIES (MFG)",
		"3. UTILITIES (UTIL)",
		"4. POWER SOURCES AND PRIME MOVERS (PS&PM)",
		"5. TRANSPORTATION (TRANS)",
		"6. COMMUNICATIONS (COMM)",
		"7. BRIDGES, TRESTLES, AND Aqueducts (BT&A)",
		"8. BUILDING TECHNOLOGY (BLD TECH)",
		"9. SPECIALIZED STRUCTURES AND OBJECTS (SPEC STRUC)"
	],

	actions: {

		removeName: function(name) {
			this.get("other_names").removeObject(name);
		},

		createName: function () {
			var newName = this.get('newName');
			if ( ! newName || ! newName.trim() ) { return; }

			if ( ! this.get('model').get('other_names')) {
				this.get('model').set('other_names', []);
			}

			this.get('model').get('other_names').addObject(newName);

			this.set('newName', '');
		},

		removeThreat: function(threat) {
			this.get("threats").removeObject(threat);
		},

		createThreat: function () {
			var newThreatName = this.get('newThreatName');
			var newThreatDate = this.get('newThreatDate');
			var newThreatDescription = this.get('newThreatDescription');
			if ( ! newThreatName || ! newThreatName.trim() ) { return; }

			var threat = {
				name: newThreatName,
				description: newThreatDescription,
				date: newThreatDate
			};

			if ( ! this.get('model').get('threats')) {
				this.get('model').set('threats', []);
			}

			this.get('model').get('threats').addObject(threat);

			this.set('newThreatName', '');
			this.set('newThreatDate', '');
			this.set('newThreatDescription', '');
		},

		removeReference: function(feature) {
			this.get("features").removeObject(feature);
		},

		createReference: function () {
			var newReferenceName = this.get('newReferenceName');
			var newReferenceHref = this.get('newReferenceHref');
			if ( ! newReferenceName || ! newReferenceName.trim() ) { return; }

			if (newReferenceHref.indexOf("http") == -1) {
				newReferenceHref = "http://" + newReferenceHref;
			}
			var reference = {
				name: newReferenceName,
				href: newReferenceHref
			};

			if ( ! this.get('model').get('references')) {
				this.get('model').set('references', []);
			}

			this.get('model').get('references').addObject(reference);

			this.set('newReferenceName', '');
			this.set('newReferenceHref', '');
		},

		removeImage: function(image) {
			this.get("images").removeObject(image);
		},

		addImage: function () {
			var _this = this;
			var input = $('<input>').attr('type', 'file');
			var id = _this.get("id");
			input.change(function() {
				var newImage = this.files[0];
				newImage.name = id + "-" + newImage.name;
					$.ajax({
					data: newImage,
					cache: false,
					processData: false,
					type: "POST",
					url: window.ENV.host + "/api/v" + window.ENV.api_version + "/upload?filename="+newImage.name,
					success: function (data) {
						var image = {
							src: data.filename,
							caption: ""
						};
						if ( ! _this.get('model').get('images')) {
							_this.get('model').set('images', []);
						}

						_this.get('model').get('images').addObject(image);
					}
				});
			});
			input.click();
		},

		removeDocument: function(document) {
			this.get("documents").removeObject(document);
		},

		addDocument: function () {
			var _this = this;
			var input = $('<input>').attr('type', 'file');
			var id = _this.get("id");
			input.change(function() {
				var newDocument = this.files[0];
				newDocument.name = id + "-" + newDocument.name;
				$.ajax({
					data: newDocument,
					cache: false,
					processData: false,
					type: "POST",
					url: window.ENV.host + "/api/v" + window.ENV.api_version + "/upload?filename="+newDocument.name,
					success: function (data) {
						var document = {
							src: data.filename,
							description: "",
							title: data.filename
						};
						if ( ! _this.get('model').get('documents')) {
							_this.get('model').set('documents', []);
						}

						_this.get('model').get('documents').addObject(document);
					}
				});
			});
			input.click();
		},

		removeFeature: function(feature) {
			this.get("model.location.geojson.features").removeObject(feature);
		},

		createFeature: function () {
			var feature;
			if (this.get("isPoint")) {
				var coordinates;
				if (this.get("isUTM")) {
					var zone = this.get('zone');
					coordinates = WGS84Util.UTMtoLL({
						geometry: {
							coordinates: [ this.get('easting'), this.get('northing') ]
						},
						properties: {
							zoneLetter: zone.substring(zone.length-1, zone.length),
							zoneNumber: zone.substring(0, zone.length-1)
						}
					}).coordinates;
				} else {
					coordinates = [ this.get("lat"), this.get("long") ];
				}

				feature = {
					"type": "Point",
					"coordinates": coordinates
				};

				var location = this.get("model.location");
				if ( ! location) {
					this.set('model.location', {});
				}
				var geojson = this.get("model.location.geojson");
				if ( ! geojson) {
					this.set('model.location.geojson', {});
				}
				var features = this.get("model.location.geojson.features");
				if ( ! features) {

					this.set('model.location.geojson.features', []);
				}

				this.get('model.location.geojson.features').addObject(feature);

				this.set('zone', '');
				this.set('easting', '');
				this.set('northing', '');
				this.set('lat', '');
				this.set('long', '');
			} else {
				feature = {
					"type": "Polygon",
					"coordinates": []
				};

				if ( ! this.get('model.location.geojson.features')) {
					this.get('model.location.geojson.features', []);
				}

				this.get('model.location.geojson.features').addObject(feature);
			}
		},

		remove: function() {
			if (! confirm("Are you sure you want to delete?")) {
				return false;
			} else {
				var _this = this;
				var model = this.get('model');
				model.destroyRecord().then(function() {
					_this.set('saved', true);
					_this.transitionToRoute('resources');
				}, function() {
					_this.set('saved', true);
					_this.transitionToRoute('resources');
				});
			}
		},

		save: function() {
			var _this = this;
			var model = this.get('model');

			model.save().then(function () {
				_this.set('newName', '');

				_this.set('newThreatName', '');
				_this.set('newThreatDate', '');
				_this.set('newThreatDescription', '');

				_this.set('newResourceName', '');
				_this.set('newResourceHref', '');

				_this.set('saved', true);
				_this.transitionToRoute('resource', model.get("id"));
			});

		}
	}

});