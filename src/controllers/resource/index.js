export default Ember.ObjectController.extend({
	authorUser: function() {
		return this.store.find('user', this.get('model.author'));
	}.property('model.author')
});