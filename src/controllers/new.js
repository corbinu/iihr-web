export default Ember.ObjectController.extend({
	needs: [ "application" ],

	actions: {

		add: function(name) {
			var _this = this;

			var resource = this.store.createRecord('resource', { name: name });
			resource.save().then(function (newDoc) {
				_this.transitionTo('resource.edit', newDoc.id);
			});
		}

	}

});