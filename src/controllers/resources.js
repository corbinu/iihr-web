export default Ember.ArrayController.extend({
	queryParams: ['term', 'page'],
	category: null,
	page: 0,
	term: "",

	search: Ember.computed.oneWay('term'),

	prev: function() {
		var page = this.get('page');
		var total = this.get('total');

		if (page === 0) {
			return -1;
		} else {
			return (page - 1);
		}
	}.property('page', 'total'),
	next: function() {
		var page = this.get('page');
		var total = this.get('total');

		if (page === parseInt((this.get('total')-1)/10)) {
			return -1;
		} else {
			return (page + 1);
		}
	}.property('page', 'total'),
	hasPrev: function() {
		return (this.get('prev') !== -1);
	}.property('prev'),
	hasNext: function() {
		return (this.get('next') !== -1);
	}.property('next'),
	pages: function() {
		var page = this.get('page');
		var total = parseInt((this.get('total')-1)/10);
		var pages = [];

		for (var i=0; i <= total; i++) {
			var current = (i === page);

			if (total < 100) {
				pages.push({
					page: i,
					current: current,
					spacer: false
				});
			} else {
				if (page < 5) {
					if (i < 8) {
						pages.push({
							page: i,
							current: current,
							spacer: false
						});
					} else if (i === 8) {
						pages.push({
							spacer: true
						});
					} else if (i > (total - 2)) {
						pages.push({
							page: i,
							current: current,
							spacer: false
						});
					}
				} else if (page > (total - 10)) {
					if (i < 2) {
						pages.push({
							page: i,
							current: current,
							spacer: false
						});
					} else if (i === 2) {
						pages.push({
							spacer: true
						});
					} else if (i > (total - 7)) {
						pages.push({
							page: i,
							current: current,
							spacer: false
						});
					}
				} else {
					if (i < 1) {
						pages.push({
							page: i,
							current: current,
							spacer: false
						});
					} else if (i === 1) {
						pages.push({
							spacer: true
						});
					} else if (((page - 4) < i) && (i < (page + 4))) {
						pages.push({
							page: i,
							current: current,
							spacer: false
						});
					} else if (i === (total - 1)) {
						pages.push({
							spacer: true
						});
					} else if (i > (total - 1)) {
						pages.push({
							page: i,
							current: current,
							spacer: false
						});
					}
				}
			}
		}

		return pages;
	}.property('total', 'page'),

	total: function() {
		return this.store.metadataFor('resource').total;
	}.property('model'),

	actions: {
		search: function () {
			this.set("page", 0);
			this.set("term", this.get("search"));
		}
	}
});