export default Ember.ObjectController.extend({

	name: function() {
		return this.get('model');
	}.property(),

	actions: {

		editName: function () {
			this.set('isEditing', true);
		},

		acceptChanges: function () {
			this.set('isEditing', false);

			if (Ember.isEmpty(this.get('model'))) {
				this.send('removeName');
			}
		},

		removeName: function () {
			var parent = this.get("target.parentController");
			parent.send('removeName', this.get('model'));
		}

	},

	isEditing: false

});