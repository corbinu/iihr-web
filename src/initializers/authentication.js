import Authenticator from '../auth/authenticator';
import Authorizer from '../auth/authorizer';

export default {
	name: 'authenticatication',
	initialize: function(container, application) {
		Ember.SimpleAuth.Session.reopen({
			account: function() {
				var accountId = this.get('user_id');
				if (!Ember.isEmpty(accountId)) {
					return container.lookup('store:main').find('user', accountId);
				}
			}.property('userId')
		});
		container.register('authenticator:iihr', Authenticator);
		container.register('authorizer:iihr', Authorizer);
		Ember.SimpleAuth.setup(container, application, {
			authorizerFactory: 'authorizer:iihr',
			crossOriginWhitelist: ["http://127.0.0.1:4000"]
		});
	}
};