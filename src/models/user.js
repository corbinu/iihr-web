var attr = DS.attr,
	hasMany = DS.hasMany,
	belongsTo = DS.belongsTo;

var User = DS.Model.extend({
	username: DS.attr('string'),
	firstname: DS.attr('string'),
	lastname: DS.attr('string'),
	email: DS.attr('string'),
	password: DS.attr('string'),

	fullname: function() {
		return this.get('firstname') + ' ' + this.get('lastname');
	}.property('firstname', 'lastname')
});

export default User;