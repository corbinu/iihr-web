var attr = DS.attr,
	hasMany = DS.hasMany,
	belongsTo = DS.belongsTo;

var Resource = DS.Model.extend({
	ref: attr('number'),
	type: attr('string'),
	name: attr('string'),
	other_names: attr(),
	location: attr(),
	description: attr('string'),
	classification: attr('string'),
	dating: attr('string'),
	materials: attr('string'),
	condition: attr('string'),
	designation: attr('string'),
	threats: attr(),
	measurements: attr('string'),
	references: attr(),
	author: attr('string'),
	/*created: attr('date'),
	updates: attr('string'),
	elements: attr('string'),*/
	images: attr(),
	documents: attr()
});

export default Resource;
