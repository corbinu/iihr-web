export default Ember.Mixin.create({

	beforeModel: function(transition) {
		if (!this.get('session').get('isAuthenticated')) {
			this.failedAuth(transition);
		}
	},

	verifyAuth: function(roles, users, transition) {
		if (transition.targetName.indexOf(this.routeName) === 0) {
			var targetController = transition.targetName.replace(this.routeName+'.',"");
			if (Object.prototype.toString.call(roles) === "[object Object]") {
				if (roles[targetController]) {
					roles = roles[targetController];
				} else {
					roles = [];
				}
			}
			if (Object.prototype.toString.call(users) === "[object Object]") {
				if (users[targetController]) {
					users = users[targetController];
				} else {
					users = [];
				}
			}
			var userRoles = this.get('session').get('content.roles').split(',');
			var userId = this.get('session').get('content.user_id');

			var auth = roles.every(function(role) {
				return userRoles.contains(role);
			});

			if (!auth) {
				auth = users.contains(userId);
			}

			if (!auth) {
				this.failedAuth(transition);
			}
		}
	},

	failedAuth: function(transition) {
		if (!this.get('session').get('isAuthenticated')) {
			transition.abort();
			this.get('session').set('attemptedTransition', transition);
			transition.send('authenticateSession');
		}
	}
});