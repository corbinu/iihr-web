import Resolver from 'ember/resolver';
import Authentication from './initializers/authentication';

var App = Ember.Application.extend({
	LOG_ACTIVE_GENERATION: true,
	LOG_MODULE_RESOLVER: true,
	LOG_TRANSITIONS: true,
	LOG_TRANSITIONS_INTERNAL: true,
	LOG_VIEW_LOOKUPS: true,
	modulePrefix: 'iihr',
	Resolver: Resolver['default']
});

App.initializer(Authentication);

export default App;
