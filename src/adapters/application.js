export default DS.RESTAdapter.extend({
	namespace: 'api/v0',
	host: window.ENV.host
});