var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function() {
	this.route('home');
	this.route('faq');
	this.route('contact');

	this.route('register');
	this.resource('user', { path: '/user/:user_id' }, function() {
		this.route('edit');
	});
	this.route('login');

	this.route('resources');

	this.route('new');

	this.resource('resource', { path: '/resource/:resource_id' }, function() {
		this.route('edit');
	});

	this.route('admin');

	this.route('loading');
	this.route('error');
});

export default Router;