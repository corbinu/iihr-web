export default Ember.View.extend({
	tagName: 'img',
	attributeBindings: ['alt', 'src', 'style'],
	didInsertElement: function() {
		var view = this;
		this.get('content').forEach(function(image, index) {
			if (index === 0) {
				if (image.src) {
					if (window.ENV.host) {
						view.set('src', window.ENV.host + "/api/v" + window.ENV.api_version + "/file/" + image.src);
					} else {
						view.set('src', "/api/v" + window.ENV.api_version + "/file/" + image.src);
					}
					view.set('alt', image.alt);
				}
			}
		});
	}
});