export default Ember.View.extend({
	templateName: 'view/map',

	didInsertElement: function() {
		var _this = this;
		var feature = this.content.features[0];
		var coordinates;
		if (feature.type == "Point") {
			coordinates = feature.coordinates;
		} else {
			coordinates = feature.coordinates[0];
		}
		var map = new GMaps({
			div: '#map',
			lat: coordinates[0],
			lng: coordinates[1],
			click: function() {
				_this.set("context.coordinates", []);
			}
		});
		this.content.features.forEach(function(feature) {
			if (feature.type == "Point") {
				map.addMarker({
					lat: feature.coordinates[0],
					lng: feature.coordinates[1],
					click: function(e) {
						_this.set("context.coordinates", [{
							latitude: e.position.A,
							longitude: e.position.k
						}]);
					}
				});
			} else {
				map.drawPolygon({
					paths: feature.coordinates,
					strokeColor: '#488DB5',
					strokeOpacity: 1,
					strokeWeight: 3,
					fillColor: '#6EB6E0',
					fillOpacity: 0.6,
					click: function(e) {
						var coordinates = [];
						this.latLngs.getAt(0).j.forEach(function(coord) {
							coordinates.push({
								latitude: coord.A,
								longitude: coord.k
							});
						});
						_this.set("context.coordinates", coordinates);
					}
				});
			}
		});

	}
});