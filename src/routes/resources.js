export default Ember.Route.extend({
	queryParams: {
		term: {
			refreshModel: true
		},
		page: {
			refreshModel: true
		}
	},
	model: function(params) {
		return this.store.find('resource', params);
	},

	actions: {
		cancel: function() {
			this.get('context').set('newName', '');
		}
	}
});