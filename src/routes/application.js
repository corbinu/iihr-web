export default Ember.Route.extend(Ember.SimpleAuth.ApplicationRouteMixin, {
	actions: {
		loading: function () {
			$('#spinner').spin('large');
			return true;
		}
	}
});