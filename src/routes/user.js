import RouteAuth from '../mixins/routeauth';

export default Ember.Route.extend(RouteAuth, {
	roles: [ "admin" ],

	model: function(params) {
		return this.store.find('user', params.user_id);
	},

	afterModel: function(user, transition) {
		this.verifyAuth(this.get('roles'), [user.get('id')], transition);
	}
});