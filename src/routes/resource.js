import RouteAuth from '../mixins/routeauth';

export default Ember.Route.extend(RouteAuth, {
	roles: {
		edit: [ "manager" ]
	},

	model: function(params) {
		return this.store.find('resource', params.resource_id);
	},

	afterModel: function(resource, transition) {
		this.verifyAuth(this.get('roles'), {
			edit: [resource.get('author')]
		}, transition);
	},

	actions: {
		willTransition: function(transition) {
			this.verifyAuth(this.get('roles'), {
				edit: [this.get('model.author')]
			}, transition);

			if (transition.pivotHandler.controller.currentPath === "resource.edit" && ! this.get("controller.saved") && ! confirm("Are you sure you want to leave the page without saving?")) {
				transition.abort();
			} else {
				this.set("controller.saved", false);
				return false;
			}
		}
	}

});