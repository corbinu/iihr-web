export default Ember.SimpleAuth.Authorizers.Base.extend({
	authorize: function(jqXHR) {
		if (this.get('session.isAuthenticated') && !Ember.isEmpty(this.get('session.key')) && !Ember.isEmpty(this.get('session.user_id'))) {
			jqXHR.setRequestHeader('Authorization', "User:" + this.get('session.user_id') + ",Key:" + this.get('session.key'));
		}
	}
});