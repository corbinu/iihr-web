export default Ember.SimpleAuth.Authenticators.Base.extend({

	restore: function(data) {
		return new Ember.RSVP.Promise(function(resolve, reject) {
			if (!Ember.isEmpty(data.key)) {
				resolve(data);
			} else {
				reject();
			}
		});
	},

	authenticate: function(credentials) {
		var endpoint = "";
		if (window.ENV.host && window.ENV.host != null) {
			endpoint = window.ENV.host;
		}
		endpoint += "/api/v" + window.ENV.api_version + "/login";
		return new Ember.RSVP.Promise(function(resolve, reject) {
			Ember.$.ajax({
				url: endpoint,
				type: 'POST',
				data: JSON.stringify({ username: credentials.identification, password: credentials.password }),
				contentType: 'application/json'
			}).then(function(response) {
				Ember.run(function() {
					resolve({ key: response.key, user_id: response.id, roles: response.roles });
				});
			}, function(xhr, status, error) {
				//var response = JSON.parse(xhr.responseText);
				Ember.run(function() {
					reject(error);
				});
			});
		});
	}
});