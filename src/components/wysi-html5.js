export default Ember.TextArea.extend({
	classNames: ['wysihtml5'],

	didInsertElement: function() {
		$('.wysihtml5').wysihtml5({
			toolbar: {
				"fa": true,
				"lists": false,
				"html": false,
				"link": false,
				"image": false,
				"color": false,
				"blockquote": false
			}
		});
	}
});